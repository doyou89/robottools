============
robotdiff.py
============

``robotdiff.py`` is a tool for comparing two or more Robot Framework output
files. The tool can be downloaded here__.

__ https://bitbucket.org/robotframework/robottools/raw/master/robot_timediff/robot_timediff.py

Synopsis
--------

.. sourcecode:: bash

    $ robot_timediff.py [options] input files

Options
-------

  -r, --report <file>       The HTML report file (created from the input files).
                            The default is *robotdiff.html*.
  -n, --name <name>         Custom names for test runs. If this option is used,
                            it must be used as many times as there are input
                            files. By default test run names are got from the
                            input file names.
  -t, --title <title>       The title for the generated diff report. The default
                            title is *Test Run Diff Report*.
  -E, --escape <what:with>  Escapes certain characters that are problematic in
                            the console. "what" is the name of the character to
                            escape and "with" is the string to escape it with.
                            The available characters to escape are same as for
                            Robot Framework. Example:
                            --escape space:_ --title My_Diff_Report
  -h, --help                Prints this usage instruction.

Description
-----------

This script compares two or more Robot Framework output files and
creates a report. This is based on robotdiff and duration field added.

Examples:

.. sourcecode:: bash

    $ robot_timediff.py output1.xml output2.xml output3.xml
    $ robot_timediff.py --name Env1 --name Env2 smoke1.xml smoke2.xml
